package kalkulatorLuasVolume;

public class luas {
       public int luasPersegi(int a,int b){
           int result = a*b;
           return result;
       }
       public double luasLingkaran(int a){
           double result = 3.14*a*a;
           return result;
       }
       public double luasSegitiga(int a, int b){
           double result = 0.5*a*b;
           return result;
       }
       public int luasPersegiPanjang(int a, int b) {
           int result = a*b;
           return result;
       }
}
