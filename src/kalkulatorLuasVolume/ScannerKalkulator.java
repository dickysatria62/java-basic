package kalkulatorLuasVolume;

import java.util.Scanner;

public class ScannerKalkulator {

    private void useLuas(String menu){
        Scanner scanner = new Scanner(System.in);
        luas ls = new luas();
        if ("1.1".equals(menu)){
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Persegi");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Sisi 1 : ");
            int a = scanner.nextInt();
            System.out.println("Masukkan Sisi 2 : ");
            int b = scanner.nextInt();
            System.out.println("--------------------------------------");
            System.out.println("Luas Persegi = "+ls.luasPersegi(a,b));
            System.out.println("--------------------------------------");
        } else if ("1.2".equals(menu)) {
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Lingkaran");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Ruji-ruji");
            int a = scanner.nextInt();
            System.out.println("Luas Lingkaran = "+ls.luasLingkaran(a));
            System.out.println("--------------------------------------");
        } else if ("1.3".equals(menu)) {
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Segitiga");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Alas");
            int a = scanner.nextInt();
            System.out.println("Masukkan Tinggi");
            int b = scanner.nextInt();
            System.out.println("Luas Segitiga = "+ls.luasSegitiga(a,b));
            System.out.println("--------------------------------------");
        } else if ("1.4".equals(menu)) {
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Persegi Panjang");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Panjang");
            int a = scanner.nextInt();
            System.out.println("Masukkan Tinggi");
            int b = scanner.nextInt();
            System.out.println("Luas Persegi Panjang = "+ls.luasPersegiPanjang(a,b));
            System.out.println("--------------------------------------");
        } else {
            System.out.println("Pilihan tidak ditemukan");
        }
    }

    private void useVolume(String menu){
        Scanner scanner = new Scanner(System.in);
        volume vm = new volume();
        if ("2.1".equals(menu)){
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Kubus");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Sisi : ");
            int a = scanner.nextInt();
            System.out.println("Volume Kubus = "+vm.volumePersegi(a));
            System.out.println("--------------------------------------");
        } else if ("2.2".equals(menu)) {
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Balok");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Panjang : ");
            int a = scanner.nextInt();
            System.out.println("Masukkan Lebar : ");
            int b = scanner.nextInt();
            System.out.println("Masukkan Tinggi : ");
            int c = scanner.nextInt();
            System.out.println("Volume Balok = "+vm.volumePersegiPanjang(a,b,c));
            System.out.println("--------------------------------------");
        } else if ("2.3".equals(menu)) {
            System.out.println("--------------------------------------");
            System.out.println("Anda Memilih Tabung");
            System.out.println("--------------------------------------");
            System.out.println("Masukkan Ruji-ruji : ");
            int a = scanner.nextInt();
            System.out.println("Masukkan Tinggi : ");
            int b = scanner.nextInt();
            System.out.println("Volume Tabung = "+vm.volumeTabung(a,b));
            System.out.println("--------------------------------------");
        }else {
            System.out.println("Pilihan tidak ditemukan");
        }
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        ScannerKalkulator sk = new ScannerKalkulator();
        System.out.println("--------------------------------------");
        System.out.println("Kalkulator Perhitungan Luas dan Volume");
        System.out.println("--------------------------------------");
        System.out.println("Menu : ");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volume");
        System.out.println("Write exit to closed");
        System.out.println("--------------------------------------");
        while (input.hasNext()) {
            String in = input.next();
            if ("exit".equals(in)) {
                break;
            } else if ("1".equals(in)) {
                System.out.println("-------------------------------");
                System.out.println("Pilih bidang yang akan dihitung");
                System.out.println("-------------------------------");
                System.out.println("1.1 Persegi ");
                System.out.println("1.2 Lingkaran ");
                System.out.println("1.3 Segitiga  ");
                System.out.println("1.4 Persegi Panjang ");
                System.out.println("Write exit to closed");
                System.out.println("-------------------------------");
                while (input.hasNext()) {
                    String menu = input.next();
                    sk.useLuas(menu);
                }

            } else if ("2".equals(in)) {
                System.out.println("-------------------------------");
                System.out.println("Pilih bidang yang akan dihitung");
                System.out.println("-------------------------------");
                System.out.println("2.1 Kubus");
                System.out.println("2.2 Tabung");
                System.out.println("2.3 Balok");
                System.out.println("Write exit to closed");
                System.out.println("-------------------------------");
                while (input.hasNext()) {
                    String menu = input.next();
                    sk.useVolume(menu);
                }
            } else {
                System.out.println("Pilihan Tidak tersedia");
            }
        }
    }
}
