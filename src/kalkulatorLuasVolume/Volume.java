package kalkulatorLuasVolume;

public class volume {
    public int volumePersegi(int a){
        int result = a*a*a;
        return result;
    }
    public double volumeTabung(int a, int b){
        double result = 3.14*a*a*b;
        return result;
    }
    public int volumePersegiPanjang(int a, int b, int c){
        int result = a*b*c;
        return result;
    }
}
