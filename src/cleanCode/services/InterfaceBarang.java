package cleanCode.services;

import cleanCode.model.Barang;

public interface InterfaceBarang {
    public Barang saveBarang (Barang obj);

    public Barang updateBarang (Barang obj);

    public Barang deletedBarang (Barang obj);
}
