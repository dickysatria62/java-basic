package cleanCode.services;

import cleanCode.model.Buku;

public interface InterfaceBuku {
    public Buku simpanBuku();
}