class helloWord{

    public static int perkalian(int a, int b){
        int c = a*b;
        return c;
    }

    public static String anyString(String f){
        f = "percobaan Yang mantap";
        return f;
    }

    public static int pertambahan (int a, int b){
        int c = a+b;
        return c;
    }

    public static int pengurangan (int a, int b){
        int c = a-b;
        return c;
    }

    public static boolean ceckPrima (int a){
        int checker = 0;
        for (int i=2; i<=a; i++)
        {
            if (a%i==0)
            {
                checker++;
            }
        }
        if (checker==1)
        {
            return true;
        }
        return false;
    }

    public static void main(String[]args){
        int a = 6;
        int b = 6;
        String nama = "Riyan Dicky Satria";
        System.out.println("hasil dari perkalian A x B adalah " + perkalian(a,b));
        System.out.println(anyString("jagung"));
        System.out.println("hasil dari perkalian A + B adalah " + pertambahan(a,b));
        System.out.println("hasil dari perkalian A - B adalah " + pengurangan(a,b));
        System.out.println("bilangan dari nilai a adalah bilangan prima/bukan " + ceckPrima(a));
    }

}
